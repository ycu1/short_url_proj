import os
import random
import string
import itertools

from flask import Flask, jsonify
from flask import make_response,request
from flask import redirect,abort
from flask_sqlalchemy import SQLAlchemy
import codec

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
base_url = 'http://127.0.0.1:8080/'
db = SQLAlchemy(app)

recycle = []
global_counter = itertools.count(1)
def next_available_counter():
    while True:
        if recycle:
            value = recycle.pop()
            yield value
        else:
            val = next(global_counter)
            if val > 56000000000:
                raise StopIteration
            else:
                yield val
id_generator = next_available_counter()

# random shuffle charset
shuffled_char_set = list(string.digits + string.ascii_letters)
random.shuffle(shuffled_char_set)
cs = codec.CodecService(charset=''.join(shuffled_char_set))


class Url(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=False)
    short_url = db.Column(db.String(120), unique=True, index=True, nullable=True)
    url = db.Column(db.String(120), nullable=False)


def json_error(json_msg=None,error_code=None):
    if json_msg is None:
        json_msg = {'error': 'Not found'}
    if error_code is None:
        error_code = 404
    return make_response(jsonify(json_msg), error_code)


@app.errorhandler(404)
def not_found(error):
    return json_error()


@app.route('/<short_url>', methods=['GET'])
def visit_short_url(short_url):
    try:
        short_url_id = cs.decode(short_url)
    except codec.CodecException:
        return json_error()

    res = Url.query.get(short_url_id)
    if res:
        return redirect(res.url)
    else:
        return json_error()


@app.route('/api/short-url/<short_url>', methods=['GET'])
def get_url(short_url):
    try:
        short_url_id = cs.decode(short_url)
    except codec.CodecException:
        return json_error()

    res = Url.query.get(short_url_id)
    if res:
        return jsonify({'url': res.url,
                        'short_url': base_url + short_url}), 201
    else:
        return json_error()



@app.route('/api/short-url/', methods=['POST'])
def post_url():
    if not request.json or not 'url' in request.json:
        return json_error(json_msg = {'message' : 'request needs to include url to be encoded'},
                          error_code = 400)

    if not request.json['url'].startswith('http://') or
        not request.json['url'].startswith('https://'):
        return json_error(json_msg = {'message' : 'url needs to start with http:// or https://'},
                          error_code = 400)

    try:
        id_ = next(id_generator)
    except StopIteration:
        return json_error(json_msg = {'message' : 'server busy'},
                          error_code = 400)

    try:
        encoded_url = cs.encode(id_)
        my_url = Url(
                     id = id_,
                     url = request.json['url'],
                     short_url = base_url + encoded_url,
                     )
        db.session.add(my_url)
        db.session.commit()
    except:
        return json_error(json_msg = {'message' : 'internal server error'},
                          error_code = 400)

    return jsonify({'url': request.json['url'],
                    'short_url': base_url + encoded_url}), 201


@app.route('/api/short-url/<short_url>', methods=['DELETE'])
def delete_url(short_url):
    try:
        short_url_id = cs.decode(short_url)
    except codec.CodecException:
        return json_error()

    try:
        Url.query.filter_by(id=short_url_id).delete()
        db.session.commit()
    except:
        return json_error(json_error = {'message' : 'internal server error'},
                          error_code = 400)
    else:
        recycle.append(short_url_id)
        return jsonify({'result': True})


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))
    db.create_all()
    app.run(host='0.0.0.0', port=port)
