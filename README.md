## SIMPLE SHORT URL DESIGN


###  Summary
```
the simple short url service is implemented in flask to
achieve a restful api interface for registering, reading
and deleting long/short url mapping information.
```


### Code Structure

```
.
├── README.md            ==> this file
├── __init__.py
├── app.py               ==> flask app
├── codec.py             ==> encode/decode logic
├── systemdesign.md      ==> system design topic on short url
└── test
    ├── __init__.py
    ├── test_app.py      ==> UT for flask app
    └── test_codec.py    ==> UT for codec
```


### API DESCRIPTION
No update operation is provided(i.e. only CRD in CRUD).

```
* registering long url
    API URL : /api/short-url/
    METHOD : POST
    DESC : registering a long url to the system, upon success,
           the web server returns 201 with json data including 
           the short url to access the long url.

* reading url
    API URL : /api/short-url/<encoded_str>
    METHOD : GET
    DESC : reading the long url mapped by encoded_str

* deleting url
    API URL : /api/short-url/<encoded_str>
    METHOD : DELETE
    DESC : deleting the long url mapped by encoded_str

* visiting url
    URL : /<encoded_str>
    METHOD : GET
    DESC : the web server will do a lookup based on the 
            encoded string, if entry exists, it will send 302 
            to redirect to the long url site, or else will 
            return 404.
```


### CODEC SERVICE

```
To achieve the mapping between long url and short url, a
codec library is implemented to map a 10 based number to
X based number (with arbitrary char set and ordering).

In the app, a charset of 62 is used, ranging digits and 
lower/uppercase letters. At program start time, the ordering
of these letters are shuffled such that external clients 
cannot predict the next url to be generated.
```


### NOTES

```
* A SQLite in-memory database is used to keep records of URL mappings.
* The system keeps track of short url being deleted and prioritizes to 
  use the recycled short urls instead of using the next largest number.
```
