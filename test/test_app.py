import pytest
import logging
import requests
import pprint

# assuming the app is running on 
# http://127.0.0.1:8080

logging.basicConfig(level=logging.DEBUG)

base_url = 'http://127.0.0.1:8080/'
api_url = 'http://127.0.0.1:8080/api/short-url/'
test_urls = [
     "https://www.ciena.com/",
     "https://www.cisco.com/",
     "https://www.google.com/maps",
     "https://www.redhat.com/en",
     "https://www.audi.com/en.html"
]
encoded_strs = []


def test_register_url():
    for test_url in test_urls:
        post_data = {"url" : test_url}
        r = requests.post(api_url,json = post_data)
        assert 200 <= r.status_code < 300
        short_url = r.json()['short_url']
        encoded_strs.append(short_url.split('/')[-1])


def test_get_url():
    for test_url,encoded_str in zip(test_urls,encoded_strs):
        r = requests.get(api_url + encoded_str)
        assert 200 <= r.status_code < 300
        assert r.json()['url'] == test_url


def test_visit_url():
    for test_url,encoded_str in zip(test_urls,encoded_strs):
        r = requests.get(base_url + encoded_str)
        assert 200 <= r.status_code < 300
        assert r.url == test_url


def test_delete_url():
    for test_url,encoded_str in zip(test_urls,encoded_strs):
        r = requests.delete(api_url + encoded_str)
        assert 200 <= r.status_code < 300
        r = requests.get(api_url + encoded_str)
        assert r.status_code == 404
