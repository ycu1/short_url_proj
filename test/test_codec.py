import codec
import string
import pytest


def test_default_encoder():
    cs = codec.CodecService()

    # invalid input
    with pytest.raises(codec.CodecException):
        assert cs.encode(-1)

    with pytest.raises(codec.CodecException):
        assert cs.encode('abc')

    with pytest.raises(codec.CodecException):
        assert cs.encode(None)

    # basic mappings
    for idx,char in enumerate(cs.charset):
        assert char == cs.encode(idx)

    # cross-validated with https://www.dcode.fr/base-n-convert
    assert cs.encode(100) == '1C'
    assert cs.encode(200) == '3e'
    assert cs.encode(1000) == 'g8'
    assert cs.encode(10000000) == 'FXsk'
    assert cs.encode(10040698) == 'G82K'
    assert cs.encode(10000000000) == 'aUKYOA'
    assert cs.encode(20030040000) == 'lRy0pi'

    # more than max_len
    assert cs.encode(80000000000) == 'pk3QwE'
    assert cs.encode(100000000000) == 'L9zO9O'


def test_default_decoder():
    cs = codec.CodecService()

    # invalid input
    with pytest.raises(codec.CodecException):
        assert cs.decode(-1)

    with pytest.raises(codec.CodecException):
        assert cs.decode(123)

    with pytest.raises(codec.CodecException):
        assert cs.decode('')

    with pytest.raises(codec.CodecException):
        assert cs.decode('+12b')

    with pytest.raises(codec.CodecException):
        assert cs.decode('-12b')

    with pytest.raises(codec.CodecException):
        assert cs.decode(None)

    # basic mappings
    for idx,char in enumerate(cs.charset):
        assert idx == cs.decode(char)

    # cross-validated with https://www.dcode.fr/base-n-convert
    assert 100 == cs.decode('1C')
    assert 200 == cs.decode('3e')
    assert 1000 == cs.decode('g8')
    assert 10000000 == cs.decode('FXsk')
    assert 10040698 == cs.decode('G82K')
    assert 10000000000 == cs.decode('aUKYOA')
    assert 20030040000 == cs.decode('lRy0pi')

    # more than max_len
    assert 23199764416 == cs.decode('1pk3QwE')
    assert 43199764416 == cs.decode('1L9zO9O')


def test_default_encoder_decoder():
    cs = codec.CodecService()
    # for value that can be fitted in max_len
    # it's guarenteed to have val = cs.decode(cs.encode(val))
    for val in [105,270,8000,10090000,90040698,
                10000005000,20030080000]:
        assert val == cs.decode(cs.encode(val))

    # for any str within len of max_len
    # it's also guarenteed to have str = cs.encode(cs.decode(str))
    for my_str in ['ciena','2D5','golang','1LMK89','KKKKKK',
                   'audi','c89',]:
        assert my_str == cs.encode(cs.decode(my_str))


def test_user_specified_charset():
    # lowercase charset
    cs = codec.CodecService(charset=string.ascii_lowercase)
    for idx,char in enumerate(cs.charset):
        assert char == cs.encode(idx)

    cs = codec.CodecService(charset=string.ascii_lowercase)
    for idx,char in enumerate(cs.charset):
        assert char == cs.encode(idx)
