## short url system design

### design backgrounds

```
In mathematical terms, we need to establish mutual mapping relationship
between the long url and short url, with the requirements that short url
MUST uniquely map to a long url at a give point of time, and the long url
ideally SHOULD map to one short url.

Since the short url has a limited set of characters, it can only represent
a finite set of possibilities. For example, given a string of at most 6 
characaters, which can be choosen from a set of 62 chars ([0-9] or [a-z] 
or [A-Z]), then we have around 56 billion urls to choose from.
```

### basic implementation

```
As demonstrated in the project code, one of a simple approach is to have a number 
generator for each new url being registered, and then map this number to an encoded 
string (e.g base62). These entries are then stored in a database such that we can 
later perform lookup based on the short url.

We could also add a LRU cache layer before the request hit the backend database to 
improve efficiency. Additionally, we could also consider using the cache layer to check
if a certain long URL already exists in the LRU cache before creating a new entry.
```

### scalability

```
In order to scale up the service to serve more concurrent requests from potentially 
different geographical locations. Several of improvements could be considered.

1. Like allocating IP addresses, we could carve our number address spaces to different 
   regions either on statically or dynamically based on demand. Thus different regions 
   will have different scope of short urls.

2. Inside each region, we could further divide the number spaces to different sections 
   for each logical or physical services to handle. (i.e. if we have two logical units,
   one would generate odd number only, the other would generate even number only)

3. For dispatching requests from clients to application servers, or from application servers
   to backend services such as cache or database servers, we should consider adding dedicated 
   load balancers.
```

### security

```
It's very important to prevent denial of service attack given the nature of this service.
We could consider rate limit the number of urls registered per hour/day from a client (
either based on registered account or source IP ). We should also check if the registered
URL are legitimate URLs in each request.
```
