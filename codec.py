import string


class CodecException(Exception):
    pass


class CodecService:

    default_charset = string.digits + string.ascii_letters
    default_max_len = 6

    def __init__(self,charset=None,max_len=None):
        self.charset = CodecService.default_charset \
                if charset is None else charset
        self.max_len = CodecService.default_max_len \
                if max_len is None else max_len
        self.base = len(self.charset)


    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'{self.charset!r}, {self.max_len!r})')


    def encode(self, number):
        """
        Encode a non-negative number to base of self.base

        :param number: int, number to be encoded
        :return: str, encoded string
        :raise CodecException:
        """
        if not isinstance(number,int) or number < 0:
            raise CodecException(
                    "only int type and value greater than "
                    "or equal to 0 can be encoded")

        if number == 0:
            return self.charset[0]

        encoded_string = []
        base = len(self.charset)
        while number:
            number, rem = divmod(number, base)
            encoded_string.append(self.charset[rem])

        encoded_string.reverse()
        if len(encoded_string) > self.max_len:
            encoded_string = encoded_string[-self.max_len:]

        return ''.join(encoded_string)


    def decode(self, encoded_string):
        """
        Decode a base of len(charset) encoded string into number

        :param encoded_string: str, the encoded string
        :return: int, decoded number
        :raise CodecException
        """
        if not isinstance(encoded_string,str) or \
            encoded_string == '':
            raise CodecException(
                    "Only non-empty str type can be decoded")

        for char in encoded_string:
            if char not in self.charset:
                raise CodecException(
                    "All the chars need to be from the charset")

        strlen = len(encoded_string)
        if strlen > self.max_len:
            encoded_string = encoded_string[-self.max_len:]
            strlen = self.max_len

        number = 0
        idx = 0
        for char in encoded_string:
            power = (strlen - (idx + 1))
            number += self.charset.index(char) * (self.base ** power)
            idx += 1

        return number
